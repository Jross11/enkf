import json


if __name__ == "__main__":
    dir_path = "example"

    config={}
    config["L_range"] = [3.1,5.1]
    config["mu"] = 700
    config["K"] = 0.11
    config["nRuns"] = 100

    config["obs_DL"] = 0.1
    config["diffusion_DL"] = 0.01
    config["Dt"] = 0.01

    with open(dir_path + '/config.json', "w") as inputJSON:
        json.dump(config, inputJSON, indent=4)